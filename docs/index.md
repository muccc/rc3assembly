# Welcome to the µrc³-Assembly.

## Proudly created by [munich ccc](https://muc.ccc.de) and [card10](https://card10.badge.events.ccc.de/)-teams

## Things you can find here:
* a somewhat realistic recreation of our hackerspace
* a subassembly for the [card10](https://card10.badge.events.ccc.de/) in the basement
* videosteam of our [flipdot-display](https://wiki.muc.ccc.de/flipdot:start)
* videostrems of our 3d-printers
* a place to get lost




